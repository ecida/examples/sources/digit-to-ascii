from Ecida import EcidaModule
import logging
import numpy as np

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


INPUT1 = "input-message"

def create_module() -> EcidaModule:
    M = EcidaModule("digittoascii", "1")
    M.add_description("gets a digit and print it as an assci art in the console.")
    # ADD MODULE INPUTS/OUTPUTS HERE
    M.add_input(INPUT1, "float64")

    return M

def main(M: EcidaModule):    
    print(f"START MODULE {M.name}:{M.version}")
    # LOGIC COMES HERE
    while True:
        message = M.pull(INPUT1)
        message = np.frombuffer(message, dtype=np.float64)
        message = int(message[0])
        visualize_digit(message)
        logger.info(" --- ")

def visualize_digit(digit):
    # ASCII art representations for digits 0 through 9
    ascii_digits = {
        0: [' 000 ', '0   0', '0   0', '0   0', ' 000 '],
        1: ['  1  ', ' 11  ', '  1  ', '  1  ', ' 111 '],
        2: [' 222 ', '2   2', '   2 ', '  2  ', '22222'],
        3: ['33333', '    3', ' 333 ', '    3', '33333'],
        4: ['   4 ', '  44 ', ' 4 4 ', '44444', '   4 '],
        5: ['55555', '5    ', '5555 ', '    5', '5555 '],
        6: [' 666 ', '6    ', '6666 ', '6   6', ' 666 '],
        7: ['77777', '   7 ', '  7  ', ' 7   ', '7    '],
        8: [' 888 ', '8   8', ' 888 ', '8   8', ' 888 '],
        9: [' 9999', '9   9', ' 9999', '    9', ' 999 '],
    }

    # Ensure the input is a single digit
    if (0 > digit and digit > 9):
        print("Please enter a single digit.")
        return

    # Retrieve the ASCII art for the digit
    digit_art = ascii_digits.get(digit)

    # Print the ASCII art line by line
    for line in digit_art:
        logger.info(line)

if __name__ == "__main__":
    M = create_module()
    M.initialize()
    main(M)